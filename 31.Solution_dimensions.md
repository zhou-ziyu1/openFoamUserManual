### 31 求解维度

OpenFOAM中所有网格都是三维的。但如果使用单层网格离散，并指定合适的`empty`或`wedge`型边界条件，则模拟可能有零维、一维、二维或三维的求解维度，或得到由OpenFOAM的`checkMesh`工具指出的求解方向。
```
Checking geometry...
    Overall domain bounding box (0 -0.1 0) (0.07 0.11 0.001)
    Mesh has 2 geometric (non-empty/wedge) directions (1 1 0)
    Mesh has 2 solution (non-empty) directions (1 1 0)
    All edges aligned with or perpendicular to non-empty directions.
```
清单183：`checkMesh`的输出片段

**零维模拟**

在零维模拟中，网格仅包含一个单元，且所有偏微分方程均简化为以时间为自变量的常微分方程。这种简化似乎太强了，但零维模拟对于测试某些模型的时间演化可能很有用。

OpenQBMM项目[^77]使用零维求解器来测试其种群平衡模型[^78]。在一个零维域中，由于不考虑对流，仅源和汇能够改变场的数值。

**一维模拟**

一维模拟只能在一个空间方向上求解或离散。当轴向流对于任何侧向流均占主导时，例如极长的管道，这是对模拟的有效简化。在OpenFOAM教程中，如教程算例*shockTube*就是一维模拟。

**二维模拟**

OpenFOAM的许多教程都是二维算例。它们足够详细，能够表现现实的流动；而它们也足够简单，能在有限的时间内完成计算。二维模拟可以是二维平面流动，例如深度远大于长度和高度的狭窄通道；也可以是轴对称流动，例如具有旋转对称性的域的剖切片。教程算例*cavity*是平面二维模拟的一个示例，算例*moveCone*则是OpenFOAM教程中轴对称模拟的一个示例。

**三维模拟**

完整的三维模拟中不存在`empty`或`wedge`型patch。

#### 31.1 对模拟进行降维求解的基本规则

**empty或wedge边界须是平面**

该规则的其中一个后果是，两个`wedge`型边界须是分开的patch。

**不可能存在单个wedge类型的patch**

这样一个同时包含剖切片两面的patch是非平面的，因为该patch的平均法线方向与它每个面的法线方向均不相同。

**empty边界须与一个坐标平面平行**

将教程算例*cavity*的网格旋转后，清单184显示了`checkMesh`的输出。最初，empty边界与$x$-$y$平面平行；用`transformPoins`旋转网格后，empty边界的法线方向变为(1,1,1)。该操作导致`checkMesh`报告0个求解方向。尝试运行算例时，`icoFoam`失败，并显示了一条相当模糊的错误消息。
```
Checking geometry...
    Overall domain bounding box (-0.0211325 -0.0211325 -0.11547) (0.084641 0.084641 0.0057735)
    Mesh has 0 geometric (non-empty/wedge) directions (0 0 0)
    Mesh has 0 solution (non-empty) directions (0 0 0)
 ***Number of edges not aligned with or perpendicular to non-empty directions: 2121
  <<Writing 882 points on non-aligned edges to set nonAlignedEdges
```
清单184：对教程*cavity*的模拟域应用从(0,0,1)到(1,1,1)的旋转后`checkMesh`的输出。

```
--> FOAM FATAL ERROR:
Continuity error cannot be removed by adjusting the outflow.
Please check the velocity boundary conditions and /or run potentialFoam to initialise the outflow.
Total flux              : 2.22507e -308
Specified mass inflow   : 0.000211325
Specified mass outflow  : 0
Adjustable mass outflow : 0

    From function bool Foam::adjustPhi(Foam::surfaceScalarField&, const volVectorField&, Foam::volScalarField &)
    in file cfdTools/general/adjustPhi/adjustPhi.C at line 107.

FOAM exiting
```
清单185：对教程*cavity*的模拟域应用从(0,0,1)到(1,1,1)的旋转后`icoFoam`的输出。

**wedge型边界不能与坐标平面平行**

轴对称模拟的网格是几何结构的离散剖切片，它在圆周方向上具有单层单元。该剖切片应跨越坐标平面。如果其中一个`wedge`型patch平行于坐标平面，则OpenFOAM会发出错误消息，与清单186所示类似。

```
--> FOAM FATAL ERROR:
wedge back plane aligns with a coordinate plane.
    The wedge plane should make a small angle (~2.5deg) with the coordinate plane and the pair of wedge planes should be symmetric about the coordinate plane.
    Normal of wedge plane is (6.451349e -20 1 5.042482e -20) , implied coordinate plane direction is (0 1 0)

    From function virtual void Foam::wedgePolyPatch::calcGeometry(Foam::PstreamBuffers&)
    in file meshes/polyMesh/polyPatches/constraint/wedge/wedgePolyPatch.C at line 110.
FOAM exiting
```
清单186：当一个`wedge`型patch平行于坐标平面时`checkMesh`的输出。